SELECT
    e1.FirstName AS SupervisorFirstName,
    e1.LastName AS SupervisorLastName,
    (
        SELECT
            CONCAT(e2.FirstName, ' ', e2.LastName)
        FROM
            Employee AS e2
        WHERE
            e1.EmployeeId = e2.ReportsTo
        LIMIT 1
    ) AS SubordinateFullName
FROM
    Employee AS e1