SELECT 
    Track.Name
FROM 
    Track
WHERE 
    Track.UnitPrice > (
        SELECT 
            MIN(UnitPrice)
		FROM Track
        WHERE 
            Track.AlbumId = Track.AlbumId
    );