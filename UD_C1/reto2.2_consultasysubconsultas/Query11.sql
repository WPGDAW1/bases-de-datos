SELECT 
    Name AS TrackName
FROM 
    Track
WHERE 
    Milliseconds > (
        SELECT 
            AVG(Milliseconds)
        FROM 
            Track
    );