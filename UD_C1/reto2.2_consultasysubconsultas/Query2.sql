SELECT DISTINCT
    Name AS ArtistName
FROM
    Artist
WHERE
    ArtistId IN (
        SELECT
            Album.ArtistId
        FROM
            Album
        JOIN
            Track ON Album.AlbumId = Track.AlbumId
        WHERE
            Milliseconds > 5 * 60 * 1000
    );