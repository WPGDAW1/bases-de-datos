SELECT
    Name AS TrackName
FROM
    Track
WHERE
    TrackId NOT IN (
        SELECT
            TrackId
        FROM
            InvoiceLine
    );