# Reto 2.2 Consultas con subconsultas
WALTER.

En este reto trabajamos con la base de datos `CHINOOK`. A continuación realizamos una serie de consultas para extraer la información pedida en cada uno de los enunciados.

El código fuente correspondiente a este reto puede consultarse en: [enlace al código fuente](https://gitlab.com/WPGDAW1/bases-de-datos.git)


## Query 1
Muestra las listas de reproducción cuyo nombre comienza por M, junto a las 3 primeras canciones de cada uno, ordenadas por álbum y por precio (más bajo primero) (usando subconsultas). Para ello, Seleccionamos el nombre de la lista de reproducción de la tabla `Playlist`, asignándolo como alias PlaylistName. Luego, utilizamos una subconsulta para obtener los nombres de las tres primeras canciones de cada lista de reproducción. La subconsulta busca los nombres de las canciones de la tabla `Track` que están asociadas a la lista de reproducción actual, ordenadas por su ID y limitadas a tres. Finalmente, ordenamos los resultados por nombre de lista de reproducción y luego por los nombres de las tres primeras canciones.


```sql
SELECT
    Playlist.Name AS PlaylistName,
    (
        SELECT
            Track.Name
        FROM
            Track
        WHERE
            PlaylistTrack.PlaylistId = Playlist.PlaylistId
        ORDER BY
            Track.TrackId
        LIMIT 3
    ) AS FirstThreeTracks
FROM
    Playlist
WHERE
    Playlist.Name LIKE 'M%'
ORDER BY
    Playlist.Name,
    FirstThreeTracks;
```


## Query 2
Muestra todos los artistas que tienen canciones con duración superior a 5 minutos.
. Para ello, Seleccionamos el nombre del artista de la tabla `Artist`, asignándolo como alias ArtistName. Utilizamos `DISTINCT` para asegurarnos de que no haya duplicados en los nombres de los artistas seleccionados. En la subconsulta, seleccionamos los IDs de los artistas de la tabla `Album` que tienen canciones con una duración superior a 5 minutos. 

```sql
SELECT DISTINCT
    Name AS ArtistName
FROM
    Artist
WHERE
    ArtistId IN (
        SELECT
            Album.ArtistId
        FROM
            Album
        JOIN
            Track ON Album.AlbumId = Track.AlbumId
        WHERE
            Milliseconds > 5 * 60 * 1000
    );
```



## Query 3
Muestra nombre y apellidos de los empleados que tienen clientes asignados.

```sql
SELECT 
    e.FirstName,
    e.LastName
FROM 
    Employee e
WHERE 
    EXISTS (
        SELECT 1
        FROM Customer c
        WHERE c.SupportRepId = e.EmployeeId
    );
```

## Query 4
Muestra todas las canciones que no han sido compradas

```sql
SELECT
    Name AS TrackName
FROM
    Track
WHERE
    TrackId NOT IN (
        SELECT
            TrackId
        FROM
            InvoiceLine
    );
```

## Query 5
Lista los empleados junto a sus subordinados (empleados que reportan a ellos).


```sql
SELECT
    e1.FirstName AS SupervisorFirstName,
    e1.LastName AS SupervisorLastName,
    (
        SELECT
            CONCAT(e2.FirstName, ' ', e2.LastName)
        FROM
            Employee AS e2
        WHERE
            e1.EmployeeId = e2.ReportsTo
        LIMIT 1
    ) AS SubordinateFullName
FROM
    Employee AS e1
```

## Query 6
Muestra todas las canciones que ha comprado el cliente Luis Rojas.

```sql
SELECT
    t.Name AS TrackName
FROM
    Track t
WHERE
    t.TrackId IN (
        SELECT
            il.TrackId
        FROM
            InvoiceLine il
        JOIN
            Invoice i ON il.InvoiceId = i.InvoiceId
        JOIN
            Customer c ON i.CustomerId = c.CustomerId
        WHERE
            c.FirstName = 'Luis' AND c.LastName = 'Rojas'
    );
```

## Query 7
Canciones que son más caras que cualquier otra canción del mismo álbum.

```sql
SELECT 
    Track.Name
FROM 
    Track
WHERE 
    Track.UnitPrice > (
        SELECT 
            MIN(UnitPrice)
		FROM Track
        WHERE 
            Track.AlbumId = Track.AlbumId
    );
```

## Query 8
Clientes que han comprado todos los álbumes de un artista específico.

```sql
SELECT 
    c.CustomerId,
    c.FirstName,
    c.LastName
FROM 
    Customer c
WHERE 
    NOT EXISTS (
        SELECT 
            *
        FROM 
            Album a
        WHERE 
            NOT EXISTS (
                SELECT 
                    *
                FROM 
                    Invoice i
                JOIN 
                    InvoiceLine il ON i.InvoiceId = il.InvoiceId
                JOIN 
                    Track t ON il.TrackId = t.TrackId
                WHERE 
                    i.CustomerId = c.CustomerId
                    AND t.AlbumId = a.AlbumId
            )
    );
```

## Query 9
Clientes que han comprado más de 20 canciones en una sola transacción

```sql
SELECT 
    CustomerId,
    FirstName,
    LastName
FROM 
    Customer
WHERE 
    CustomerId IN (
        SELECT 
            CustomerId
        FROM 
            Invoice
        GROUP BY 
            CustomerId
        HAVING 
            COUNT(*) > 20
    );
```

## Query 10
Muestra las 10 canciones más compradas

```sql
SELECT 
    Name AS TrackName
FROM 
    Track
WHERE 
    TrackId IN (
        SELECT 
            TrackId
        FROM 
            InvoiceLine
        GROUP BY 
            TrackId
        ORDER BY 
            COUNT(*) DESC
        LIMIT 10
    );
```

## Query 11 
Muestra las canciones con una duración superior a la media.

```sql
SELECT 
    Name AS TrackName
FROM 
    Track
WHERE 
    Milliseconds > (
        SELECT 
            AVG(Milliseconds)
        FROM 
            Track
    );
```


## Query 12
Para demostrar lo bueno que es nuestro servicio, muestra el número de países
donde tenemos clientes, el número de géneros músicales de los que disponemos y
el número de pistas.

```sql
SELECT 
    (SELECT COUNT(DISTINCT Country) FROM Customer) AS NumCountries,
    (SELECT COUNT(DISTINCT GenreId) FROM Track) AS NumGenres,
    (SELECT COUNT(*) FROM Track) AS NumTracks;
```

## Query 13
Canciones vendidas más que la media de ventas por canción.


```sql
SELECT 
    Name AS TrackName
FROM 
    Track
WHERE 
    TrackId IN (
        SELECT 
            TrackId
        FROM 
            InvoiceLine
        GROUP BY 
            TrackId
        HAVING 
            COUNT(*) > (
                SELECT 
                    AVG(NumSales)
                FROM (
                    SELECT 
                        COUNT(*) AS NumSales
                    FROM 
                        InvoiceLine
                    GROUP BY 
                        TrackId
                ) AS AvgSales
            )
    );
```
## FUNCIONES CON CASE 

## Query 14 


```sql
USE Chinook;

SELECT FirstName, LastName, Country, 
	CASE
		WHEN c.Country = "USA" THEN 'LOCAL' 
        WHEN c.Country != "USA" THEN 'INTERNACIONAL'
        WHEN c.Country IS NULL THEN 'Desconocido'
	END AS CountryText
FROM Customer AS c;
```

## Query 15


```sql

```

## Query 16


```sql
```

## Query 17


```sql

```