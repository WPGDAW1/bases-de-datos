USE Chinook;

SELECT FirstName, LastName, Country, 
	CASE
		WHEN c.Country = "USA" THEN 'LOCAL' 
        WHEN c.Country != "USA" THEN 'INTERNACIONAL'
        WHEN c.Country IS NULL THEN 'Desconocido'
	END AS CountryText
FROM Customer AS c;