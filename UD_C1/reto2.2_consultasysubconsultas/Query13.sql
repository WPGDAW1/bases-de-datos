SELECT 
    Name AS TrackName
FROM 
    Track
WHERE 
    TrackId IN (
        SELECT 
            TrackId
        FROM 
            InvoiceLine
        GROUP BY 
            TrackId
        HAVING 
            COUNT(*) > (
                SELECT 
                    AVG(NumSales)
                FROM (
                    SELECT 
                        COUNT(*) AS NumSales
                    FROM 
                        InvoiceLine
                    GROUP BY 
                        TrackId
                ) AS AvgSales
            )
    );