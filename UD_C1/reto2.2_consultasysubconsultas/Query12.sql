SELECT 
    (SELECT COUNT(DISTINCT Country) FROM Customer) AS NumCountries,
    (SELECT COUNT(DISTINCT GenreId) FROM Track) AS NumGenres,
    (SELECT COUNT(*) FROM Track) AS NumTracks;