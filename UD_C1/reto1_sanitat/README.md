# Reto 1: Consultas básicas

WALTER.

En este reto trabajamos con la base de datos `sanitat`, que nos viene dada en el fichero `sanitat.sql`. A continuación realizamos una serie de consultas para extraer la información pedida en cada uno de los enunciados.

El código fuente correspondiente a este reto puede consultarse en: https://gitlab.com/WPGDAW1/bases-de-datos.git

## Query 1
Para seleccionar el número, nombre y teléfono de todos los hospitales existentes, seleccionaremos estos tres atributos, que se corresponden con las columnas `HOSPITAL_COD`, `NOM`, y `TELEFON`, respectivamente, de la tabla `HOSPITAL`. Lo llevaremos a cabo con la siguiente sentencia SQL:

```sql
SELECT HOSPITAL_COD,NOM,TELEFON FROM HOSPITAL;
```


## Query 2
Para seleccionar el número, nombre y teléfono de todos los hospitales existentes, correspondidas con las columnas `HOSPITAL_COD`, `NOM`, y `TELEFON`, respectivamente, de la tabla `HOSPITAL`. Además, deberemos realizar una busqueda con el filtro **LIKE** para realizar esta busqueda en base a tener una letra a en la segunda posición del nombre. A continuación, la sentencia SQL:

```sql
SELECT HOSPITAL_COD, NOM, ADRECA, TELEFON FROM HOSPITAL WHERE NOM LIKE '_a%';
```

## Query 3
Para seleccionar el código hospital, código sala, número empleado y apellido de los trabajadores existentes, correspondidas con las columnas `HOSPITAL_COD`, `SALA_COD`, `EMPLEAT_NO`, `COGNOM`, respectivamente, de la tabla `PLANTILLA`. A continuación, la sentencia SQL:


```sql
SELECT HOSPITAL_COD, SALA_COD, EMPLEAT_NO, COGNOM FROM PLANTILLA;
```

## Query 4
Para seleccionar el código hospital, código sala, número empleado y apellido de los trabajadores existentes, correspondidas con las columnas `HOSPITAL_COD`, `SALA_COD`, `EMPLEAT_NO`, `COGNOM`, respectivamente, de la tabla `PLANTILLA`. A continuación, la sentencia SQL:


```sql
SELECT HOSPITAL_COD AS "Código", 
SALA_COD, 
EMPLEAT_NO, 
COGNOM FROM PLANTILLA
WHERE PLANTILLA.TORN != "N";
```


## Query 5
Para seleccionar el código hospital, código sala, número empleado y apellido de los trabajadores existentes, correspondidas con las columnas `HOSPITAL_COD`, `SALA_COD`, `EMPLEAT_NO`, `COGNOM`, respectivamente, de la tabla `PLANTILLA`. A continuación, la sentencia SQL:


```sql
SELECT
DATA_NAIX AS "F_NACIMIENTO",
COGNOM
FROM MALALT
WHERE YEAR(DATA_NAIX) LIKE "%1960";
```


## Query 6
Para seleccionar el código hospital, código sala, número empleado y apellido de los trabajadores existentes, correspondidas con las columnas `HOSPITAL_COD`, `SALA_COD`, `EMPLEAT_NO`, `COGNOM`, respectivamente, de la tabla `PLANTILLA`. A continuación, la sentencia SQL:


```sql
SELECT
DATA_NAIX AS "F_NACIMIENTO",
COGNOM
FROM MALALT
WHERE YEAR(DATA_NAIX) >= "1960";
```