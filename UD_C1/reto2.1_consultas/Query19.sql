SELECT a.Title AS "Género", COUNT(i.Quantity) AS "Total compras" 
FROM Album AS a
JOIN Track AS t
JOIN InvoiceLine AS i
ON a.AlbumId = t.AlbumId
AND t.TrackId = i.TrackId
GROUP BY a.Title
ORDER BY COUNT(i.Quantity) DESC
LIMIT 6;