USE Chinook;
SELECT COUNT(t.TrackId) AS "Canciones", 
g.Name FROM Track as t
JOIN Genre as g
ON t.GenreId = g.GenreId
GROUP BY g.Name
ORDER BY g.Name;