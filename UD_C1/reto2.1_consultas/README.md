# Reto 2.1 Consultas con funciones de agregaciónTasca

WALTER.

En este reto trabajamos con la base de datos `CHINOOK`. A continuación realizamos una serie de consultas para extraer la información pedida en cada uno de los enunciados.

El código fuente correspondiente a este reto puede consultarse en: [enlace al código fuente](https://gitlab.com/WPGDAW1/bases-de-datos.git)


## Query 1
Para que podamos encontrar a todos los clientes de Francia, selecciono la tabla `Customer` y seguido, los campos `FirstName` y `LastName`. Después, realizo un filtro sobre el campo `Country` para la busqueda específica de Francia.

```sql
USE Chinook;
SELECT FirstName AS "Nombre",
LastName AS "Apellido"
FROM Customer
WHERE Country = "France";
```


## Query 2
Para que podamos sacar todos las facturas del primer trimestre del año, selecciono la tabla `Invoice` y seguido, los campos `InvoiceId` y `InvoiceDate` . Después, realizo un filtro sobre el campo `InvoiceDate` usando la función `YEAR()` y `YEAR(now())` para buscar sobre el año actual todas sus facturas, lo mismo con el mes `Month()` para especificar que el mes esté entre `1` y `3` mediante la función `BETWEEN`:

```sql
USE Chinook;
SELECT InvoiceId AS "Factura", 
InvoiceDate AS "Fecha" FROM Invoice
WHERE YEAR(InvoiceDate) = YEAR(now())
AND MONTH(InvoiceDate) BETWEEN 1 AND 3 ;
```


## Query 3
Para sacar las canciones del grupo AC/DC, selecciono la tabla `Track`, junto con el campo `Name`. Después, creo un filtro sobre el campo `Composer` para que buscar al compositor `AC/DC`.

```sql
USE Chinook;
SELECT Name AS "Cancion" 
FROM Track
WHERE Composer = "AC/DC";
```

## Query 4
Para mostrar las 10 canciones que más tamaño ocupan, selecciono la tabla `Track`, seguido del campo `Name` y ordenando con `ORDER BY` junto con el campo `Bytes` para buscar los tamaños, de manera descendente `DESC` y limito para que solo salgan 10 valores `LIMIT 10`.
```sql
USE Chinook;
SELECT Name AS "Cancion" 
FROM Track
ORDER BY Bytes DESC
LIMIT 10;
```

## Query 5
Para mostrar todos los paises donde hay clientes, selecciono la la tabla `Customer`, seguido de los campos `Country` junto con  `DISTINCT` para evitar que se repitan los países del campo mencionado. Después, creo un filtro sobre el campo `Country` para todos los valores que no sean nulo (`IS NOT NULL`).

```sql
USE Chinook;
SELECT DISTINCT(Country) 
FROM Customer
WHERE Country IS NOT NULL;
```

## Query 6
Para mostrar todos los géneros musicales, selecciono la tabla `Genre`, seguido del campo `Name`

```sql
USE Chinook;
SELECT Name AS "Nombre" FROM Genre;
```

### COMENZAMOS CON JOIN

## Query 7
Para mostrar todos los artistas y sus álbumes, selecciono la tabla `Album`, seguido del campos `Name` y `Title`. Luego, realizo un `JOIN` del campo `Artista` con `Album` comparandolas con `ON` sobre los campos `art.ArtistId = alb.ArtistId` para finalmente ordenar por nombres de Artistas (`ar.Name`).

```sql
USE Chinook;
SELECT art.Name AS "Artista", alb.Title AS "Álbum"
FROM Artist AS art
JOIN Album AS alb
ON art.ArtistId = alb.ArtistId
ORDER BY art.Name;
```

## Query 8
Para la busqueda de los 15 empleados más jovenes, selecciono la tabla `Employee`, seguido de los campos `FirstName` y `BirthDate`. Además, realizo un `JOIN` con `Employee` comparandolas con `ON` sobre los campos `em.ReportsTo = sup.EmployeeId`, ordeno por `em.BirthDate` de manera descendente `DESC` y limito por 5 valores (`LIMIT 5`):

```sql
USE Chinook;
SELECT em.FirstName AS "Empleado", em.BirthDate, 
sup.Firstname AS "Supervisor" 
FROM Employee AS em
JOIN Employee AS sup
ON em.ReportsTo = sup.EmployeeId
ORDER BY emp.BirthDate DESC
LIMIT 5;
```

## Query 9
Para mostrar todas las facturas de los clientes berlineses, selecciono la tabla `Invoice`, seguido de los campos `InvoiceDate`, `BillingAddress` y `Total`. Además, mediante `JOIN` , seleccionaré los campos `Firstname`, `Lastname`, `PostalCode` y `Country` de la tabla `Customer`, comparandolas con `ON` sobre los campos `f.CustomerId = c.CustomerId` y filtro sobre la columna `c.City` para todos aquellos valores que  sean (`Berlin`) :

```sql
USE Chinook;
SELECT f.InvoiceDate AS "Fecha factura", c.FirstName AS "Nombre", 
c.Lastname AS "Apellido",
f.BillingAddress AS "Dirección de facturación", 
c.PostalCode AS "Código postal",
c.Country AS "País", f.Total AS "Importe"
FROM Invoice AS f
JOIN Customer AS c
ON f.CustomerId = c.CustomerId
WHERE c.City = "Berlin";
```

## Query 10
Para mostrar las listas de reproducción, selecciono la tabla `Playlist`, seguido de los campos `Name` y `Milliseconds`. A su vez, mediante `JOIN`, seleccionaré la tabla `Album` seguido del campo `Title`, mediante `JOIN`s necesarios de las tablas `Playlist(List)` , `PlaylistTrack(pt)` , `Track(t)`  y `Album(a)`  comparandolas (`ON`) sobre las columna `f.CustomerId = c.CustomerId` , `pt.TrackId = t.TrackId` y `t.AlbumId = a.AlbumId`. Después, creo un filtro sobre el campo `List.Name` para todos aquellos valores que empiecen por la letra C (`LIKE "C%"`) y finalmente ordeno (`ORDER BY`) por `t.AlbumId, t.Milliseconds Desc` :

```sql
USE Chinook;
SELECT List.Name AS "PlayList", t.Name AS "Cancion", 
a.Title AS "Álbum", t.Milliseconds AS "Duración"
FROM Playlist AS List
JOIN PlaylistTrack AS pt
JOIN Track AS t
JOIN Album AS a
ON List.PlaylistId = pt.PlaylistId
AND pt.TrackId = t.TrackId
AND t.AlbumId = a.AlbumId
WHERE List.Name LIKE "C%"
ORDER BY t.AlbumId, t.Milliseconds DESC;
```

## Query 11 
Para mostrar los clientes cuyas compras realizados son +10€, selecciono la tabla `Invoice`, seguido de los campos `Total`, `FirstName` y `LastName`. También, realizo un `JOIN` sobre las tablas `Customer` y `Invoice` comparandolas con `ON` sobre los campos `c.CustomerId = i.CustomerId`, y creando un filtro sobre el campo `Total` para todos aquellos valores que sean `> 10` ( y así saber de las compras con ese precio superior ) y finalmente ordeno con `ORDER BY c.LastName DESC` :

```sql
USE Chinook;
SELECT c.Firstname AS "Nombre", c.Lastname AS "Apellido", 
i.Total AS "Importe"
FROM Customer AS c
JOIN Invoice AS i
ON c.CustomerId = i.CustomerId
WHERE i.Total > 10
ORDER BY c.LastName DESC;
```
### COMENZAMOS CON FUNCIONES DE AGREGACIÓN

## Query 12
Para mostrar el importe medio, mínimo y máximo de cada factura, usaré las funciones `AVG(media)`, `MIN(mínimo)` y `MAX(máximo)`. También, seleccionaré la tabla `Invoice` seguido del campo `Total` usando cada función de agregación mencionadas anteriormente.

```sql
USE Chinook;
SELECT AVG(Total) AS "Media",
MIN(Total) AS "Mínimo",
MAX(Total) AS "Máximo" 
FROM Invoice;
```

## Query 13
Para mostrar el número total de artistas, selecciono la tabla `Artist`, seguido del campo `Name`. Con un `COUNT()`, averiguaremos el conteo total de artistas.

```sql
USE Chinook;
Select COUNT(ArtistId) AS "Nº Artistas" 
FROM Artist;
```

## Query 14 
Para mostrar el número de canciones del álbum “Out Of Time”, selecciono la tabla `Track`, seguido del campo `Name` junto con un `COUNT()` del mismo campo. Luego realizo un `JOIN` de las tablas `Track(t)` y `Album(a)` comparandolas con (`ON`) sobre `t.AlbumId = a.AlbumId` y hago un filtro sobre la columna `a.Title` para que todos los valores sean `= "Out Of Time"`:

```sql
SELECT COUNT(t.TrackId) AS "Canciones", 
a.Title FROM Track AS t
JOIN Album AS a
ON t.AlbumId = a.AlbumId
WHERE a.Title = "Out Of Time"
```

## Query 15
Para mostrar el número de países donde tenemos clientes, selecciono la tabla `Customer`, seguido del campo `Country` junto con la función `COUNT` (para el conteo total). También realicé un filtro sobre el mismo campos para todos aquellos valores que no sean nulos (`IS NOT NULL`):

```sql
USE Chinook;
SELECT COUNT(Country) AS "Nº Paises" FROM Customer
WHERE Country IS NOT NULL;
```

## Query 16
Para mostrar el número de canciones de cada género, selecciono la tabla `Track`, seguido de los campos `TrackId` junto a la función `COUNT` (para contear el total de canciones). También, para esta ocasión, seleccionaré la tabla `Genre` junto con el campo `Name`. Entre ellas, realizo un `JOIN` comparando con `ON` sobre los campos `t.GenreId = g.GenreId`. Luego agrupo con `GROUP BY` por `g.Name` y ordeno con `ORDER BY`por `g.Name`.

```sql
USE Chinook;
SELECT COUNT(t.TrackId) AS "Canciones", 
g.Name FROM Track as t
JOIN Genre as g
ON t.GenreId = g.GenreId
GROUP BY g.Name
ORDER BY g.Name;
```

## Query 17
Para mostrar los álbumes, selecciono la tabla `Album` seguido del campo `Title` y también la tabla `Track` seguido del campo `TrackId` agregandole la función `COUNT` (para contar albumes). Luego, realizo un `JOIN` sobre la tabla `Album` y `Track` comparando con `ON` sobre los campos `a.AlbumId = t.AlbumId`. Después, agrupo con `GROUP BY` en `a.Title` y ordeno con `ORDER BY`sobre el conteo de canciones de manera `DESC`:

```sql
SELECT a.Title AS "Álbum", COUNT(t.TrackId) AS "Canciones"
FROM Album AS a
JOIN Track AS t
ON a.AlbumId = t.AlbumId
GROUP BY a.Title
ORDER BY COUNT(t.trackID) DESC;
```

## Query 18 
Para encontrar los géneros musicales más populares, selecciono la tabla `Genre`, seguido del campo `Name`. También selecciono la tabla `InvoiceLine`, seguido del campo `Quantity`. Luego realizo un `JOIN` entre las tablas `Genre` , `Track` y `InvoiceLine` comparando con `ON` sobre los campos `g.GenreId = t.GenreId` y `t.TrackId = i.TrackId`. Dicho esto, agrupamos con `GROUP BY` de `g.Name` (`GROUP BY g.Name`) y finalmente, ordeno con `ORDER BY` sobre COUNT(i.Quantity) de manera `DESC` :

```sql
USE Chinook;
SELECT g.Name AS "Genero", COUNT(i.Quantity) AS "Total compras" 
FROM Genre AS g
JOIN Track AS t
JOIN InvoiceLine AS i
ON g.GenreId = t.GenreId
AND t.TrackId = i.TrackId
GROUP BY g.Name
ORDER BY COUNT(i.Quantity) DESC;
```

## Query 19
Para listar los 6 álbumes que acumulan más compras, selecciono la tabla `Album`, seguido del campo `Title`. También selecciono la tabla `InvoiceLine`, seguido del campo `Quantity`. Después,  realizo un `JOIN` entre las tablas `Album` , `Track` y `InvoiceLine(i)` comparando con `ON` sobre los campos `a.AlbumId = t.AlbumId` y `t.TrackId = i.TrackId`, agrupando con `GROUP BY` en `a.Title`, ordenando con `ORDER BY` sobre COUNT(i.Quantity) de manera `DESC` y limitando solo a 6 valores con `LIMIT 6` :

```sql
SELECT a.Title AS "Género", COUNT(i.Quantity) AS "Total compras" 
FROM Album AS a
JOIN Track AS t
JOIN InvoiceLine AS i
ON a.AlbumId = t.AlbumId
AND t.TrackId = i.TrackId
GROUP BY a.Title
ORDER BY COUNT(i.Quantity) DESC
LIMIT 6;
```

## Query 20
Para mostrar los países en los que tenemos al menos 5 clientes, selecciono la tabla `Customer`, seguido del campo `CustomerID` que uniremos con la función `COUNT` ( para contar los clientes ) y también el campo `Country`. Para luego mostrar directo el nombre de países, agrupamos con `GROUP BY` en `Country` y luego usaré `HAVING` para que así recoja todo del `GROUP BY` y lo una con `COUNT(CustomerID) >= 5` para sacar al menos esos 5 valores (5 clientes).

```sql
USE Chinook;
SELECT COUNT(CustomerID), Country
FROM Customer
GROUP BY Country
HAVING COUNT(CustomerID) >= 5;
```
