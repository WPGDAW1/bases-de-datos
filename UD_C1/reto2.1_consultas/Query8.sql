USE Chinook;
SELECT em.FirstName AS "Empleado", em.BirthDate, 
sup.Firstname AS "Supervisor" 
FROM Employee AS em
JOIN Employee AS sup
ON em.ReportsTo = sup.EmployeeId
ORDER BY emp.BirthDate DESC
LIMIT 5;