SELECT a.Title AS "Álbum", COUNT(t.TrackId) AS "Canciones"
FROM Album AS a
JOIN Track AS t
ON a.AlbumId = t.AlbumId
GROUP BY a.Title
ORDER BY COUNT(t.trackID) DESC;