USE Chinook;
SELECT art.Name AS "Artista", alb.Title AS "Álbum"
FROM Artist AS art
JOIN Album AS alb
ON art.ArtistId = alb.ArtistId
ORDER BY art.Name;