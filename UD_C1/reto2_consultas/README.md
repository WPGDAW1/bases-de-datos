# Reto 1: Consultas básicas

WALTER.

En este reto trabajamos con la base de datos `empresa y videoclub`, que nos viene dada en el fichero `sanitat.sql y videoclub.sql`. A continuación realizamos una serie de consultas para extraer la información pedida en cada uno de los enunciados.

El código fuente correspondiente a este reto puede consultarse en: [enlace al código fuente](https://gitlab.com/WPGDAW1/bases-de-datos.git)

## Query 1
Para seleccionar el código y descripción de todos los productos existentes, seleccionaremos estos dos atributos, que se corresponden con las columnas `PROD_NUM` y `DESCRIPCIO`, respectivamente, de la tabla `empresa.PRODUCTE`. Lo llevaremos a cabo con la siguiente sentencia SQL:

```sql
use empresa;

SELECT PROD_NUM, DESCRIPCIO FROM PRODUCTE;
```

## Query 2
Para seleccionar el código y descripción de todos los productos existentes, seleccionaremos estos dos atributos, que se corresponden con las columnas `PROD_NUM` y `DESCRIPCIO`, respectivamente, de la tabla `empresa.PRODUCTE`. Además de ello, buscaremos una selección concreta donde muestren todos los productos que tengan tennis en la descripción (mediante **WHERE**). Lo llevaremos a cabo con la siguiente sentencia SQL:

```sql
use empresa;

SELECT PRODUCTE.PROD_NUM, PRODUCTE.DESCRIPCIO FROM PRODUCTE
WHERE DESCRIPCIO LIKE "%TENNIS%";

```

## Query 3
Para seleccionar el código, nombre, área y teléfono de todos los clientes existentes, seleccionaremos estos cuatro atributos, que se corresponden con las columnas `CLIENT_COD` , `NOM`, `AREA` y `TELEFON` respectivamente, de la tabla `empresa.CLIENT`. Lo llevaremos a cabo con la siguiente sentencia SQL:

```sql
use empresa;

SELECT CLIENT_COD, NOM, AREA, TELEFON  FROM CLIENT;

```

## Query 4
Para seleccionar el código, nombre y ciudad de todos los clientes existentes, seleccionaremos estos tres atributos, que se corresponden con las columnas `CLIENT_COD` , `NOM`, `AREA`, respectivamente, de la tabla `empresa.CLIENT`. Además de ello, realizaremos una busqueda que no muestre a los clientes que no sean del área 636 ( mediante **WHERE**) Lo llevaremos a cabo con la siguiente sentencia SQL:

```sql
use empresa;

SELECT CLIENT_COD, NOM, CIUTAT  FROM CLIENT
WHERE AREA != 636;

```

## Query 5
Para seleccionar el código, fechas de orden y de envío de todos los pedidos existentes, seleccionaremos estos tres atributos, que se corresponden con las columnas `CLIENT_COD` , `COM_DATA` y `DATA_TRAMESA`, respectivamente, de la tabla `empresa.COMANDA`. Lo llevaremos a cabo con la siguiente sentencia SQL:

```sql
use empresa;

SELECT CLIENT_COD, COM_DATA, DATA_TRAMESA FROM COMANDA;

```

## Query 6
Para seleccionar el código, nombre, área y teléfono de todos los clientes existentes, seleccionaremos estos dos atributos, que se corresponden con las columnas `NOM`y `TELEFON` respectivamente, de la tabla `videoclub.CLIENT`. Lo llevaremos a cabo con la siguiente sentencia SQL:

```sql
USE videoclub;
SELECT NOM, TELEFON FROM CLIENT;

```

## Query 7
Para seleccionar la fecha y importa de todas las facturas existentes, seleccionaremos estos dos atributos, que se corresponden con las columnas `DATA` y `IMPORT` respectivamente, de la tabla `videoclub.FACTURA`. Lo llevaremos a cabo con la siguiente sentencia SQL:

```sql
USE videoclub;
SELECT DATA, IMPORT FROM FACTURA;

```

## Query 8
Para seleccionar código de factura y descripción de toda la lista de facturas, seleccionaremos estos dos atributos, que se corresponden con las columnas `DESCRIPCIO` y `CODIFACTURA` respectivamente, de la tabla `videoclub.DETALLFACTURA`. Además de ello, queremos buscar (mediante **WHERE**) concretamente, los detalles de la factura 3. Lo llevaremos a cabo con la siguiente sentencia SQL:

```sql
USE videoclub;
SELECT Descripcio, CodiFactura FROM DETALLFACTURA
WHERE CodiFactura = 3;

```

## Query 9
Para seleccionar la lista de facturas ordenadas en decreciente por importe , seleccionaremos este atributo, que se corresponden con la columna `IMPORT` respectivamente, de la tabla `videoclub.FACTURA`. Además de ello, para ordenar de forma decreciente por importe, seleccionamos **ORDER BY** IMPORT Lo llevaremos a cabo con la siguiente sentencia SQL:

```sql
USE videoclub;
SELECT IMPORT FROM FACTURA
ORDER BY IMPORT;

```

## Query 10
Para seleccionar el nombre de todos los actores existentes, seleccionaremos este atributo, que se corresponden con la columna `NOM`, respectivamente, de la tabla `videoclub.ACTOR`. Además de ello, se nos pide buscar concretamente todos los actores cuyo nombre contenga **X** ( mediante **WHERE y LIKE**) Lo llevaremos a cabo con la siguiente sentencia SQL:

```sql
USE videoclub;
SELECT NOM FROM ACTOR
WHERE NOM LIKE "X%";

```



